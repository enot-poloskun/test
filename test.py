import unittest
from unittest import TestCase
from unittest.mock import patch
from des import DES, DECRYPT


class TestDES(TestCase):
    @patch('des.DES.shift', return_value="100011")
    def test_shift1(self, shift):
        self.assertEqual(shift("111000", 2), "100011")

    @patch('des.DES.shift', return_value="111000")
    def test_shift2(self, shift):
        self.assertEqual(shift("111000", 0), "111000")

    @patch('des.DES.shift', return_value="001110")
    def test_shift3(self, shift):
        self.assertEqual(shift("111000", -2), "001110")

    @patch('des.DES.binvalue', return_value="001")
    def test_binvalue1(self, binvalue):
        self.assertEqual(binvalue(1, 3), "001")

    @patch('des.DES.binvalue', return_value="101")
    def test_binvalue2(self, binvalue):
        self.assertEqual(binvalue(5, 3), "101")

    @patch('des.DES.binvalue', return_value="1000001")
    def test_binvalue3(self, binvalue):
        self.assertEqual(binvalue('A', 7), "1000001")

    @patch('des.DES.binvalue', return_value="001000001")
    def test_binvalue4(self, binvalue):
        self.assertEqual(binvalue('A', 9), "001000001")

    @patch('des.DES.to_bit_array',
           return_value=[0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1])
    def test_to_bit_array1(self, to_bit_array):
        self.assertEqual(to_bit_array("abc"), [0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1])

    @patch('des.DES.to_bit_array',
           return_value=[0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1])
    def test_to_bit_array2(self, to_bit_array):
        self.assertEqual(to_bit_array([1, 2, 3]),
                         [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1])

    @patch('des.DES.to_bytestring', return_value= b'\x01\x02\x03')
    def test_to_bytestring1(self, to_bytestring):
        self.assertEqual(to_bytestring([0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1]),
                         b'\x01\x02\x03')

    @patch('des.DES.to_bytestring', return_value=b'abc')
    def test_to_bytestring2(self, to_bytestring):
        self.assertEqual(to_bytestring([0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1]),
                         b'abc')


class IntegrTests(unittest.TestCase):
    des = DES([1, 2, 3, 4, 5, 6, 7, 8])

    def test1(self):
        self.assertEqual(self.des.process_string("abcdefgh"), b'\xa5\x13T\xe1M\x90;\xef')

    def test2(self):
        self.assertEqual(self.des.process_string(""), b'')

    def test3(self):
        self.assertEqual(self.des.process_string(b'\x01\x02\x03\x04\x05\x06\x07\x08'), b'\xbb[\xe9|\xfa\xb6\x91v')

    def test4(self):
        self.assertEqual(self.des.process_string(b'\xa5\x13T\xe1M\x90;\xef', DECRYPT), b'abcdefgh')

    def test5(self):
        self.assertEqual(self.des.process_string(b'\xbb[\xe9|\xfa\xb6\x91v', DECRYPT),
                         b'\x01\x02\x03\x04\x05\x06\x07\x08')